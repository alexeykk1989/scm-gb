#!/usr/bin/env bash

ansible-playbook -vv \
	-i inventory/dev/ \
	-c local \
	-k -K \
	pb.yml "$@"
