#!/usr/bin/env bash

ansible-playbook -vv \
	-i inventory/dev/hosts.yml \
	-c local \
	-b \
	-k -K \
	--ask-vault-pass \
	less3.yml "$@"
